# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from bisect import bisect_left
import cairo
import gi
import math
import struct
import traceback

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

import cmd
import utils
from utils import debug

class HexView(Gtk.Box):
    def __init__(self, page, settings, data_iter, lines=None, comments=None, offset=0):
        super().__init__(orientation="vertical")
        self.page = page
        self.hdr = Gtk.DrawingArea()
        box = Gtk.Box(0, 0) # horizontal, no spacing
        self.body = Gtk.DrawingArea()
        self.scrbar = Gtk.Scrollbar.new(1) # vertical
        self.scrbar.set_redraw_on_allocate(False)
        self.body_vadj = self.scrbar.get_adjustment()
        box.pack_start(self.body, True, True, 0)
        box.pack_start(self.scrbar, False, False, 0)
        self.body.add_events(Gdk.EventMask.SCROLL_MASK | Gdk.EventMask.KEY_PRESS_MASK | Gdk.EventMask.KEY_RELEASE_MASK | Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK | Gdk.EventMask.POINTER_MOTION_MASK)
        self.body.set_can_focus(True)
        self.body.set_focus_on_click(True)

        self.body.connect("scroll-event", self.on_scroll)
        self.body.connect("key-press-event", self.on_keypress)
        self.body.connect("key-release-event", self.on_keyrelease)
        self.body.connect("button-press-event", self.on_btnpress)
        self.body.connect("button-release-event", self.on_btnrelease)
        self.body.connect("motion-notify-event", self.on_btnmove)
        self.body.grab_focus()

        self.pack_start(self.hdr, False, False, 1)
        self.pack_start(box, True, True, 0)

        builder = Gtk.Builder.new_from_file("ui/hv_widgets.ui")
        self.comment_po = builder.get_object("comment_po")
        self.comment_entry = builder.get_object("comment_entry")
        self.comment_entry.connect("key-press-event", self.on_po_entry_keypress)
        self.comment_clr_btn = builder.get_object("comment_clr_btn")
        self.comment_clr_btn.connect("color-set", self.on_clr_set)

        self.mode_po = builder.get_object("mode_po")
        self.mode_spn_btn = builder.get_object("mode_spn_btn")
        self.mode_spn_btn.connect("key-press-event", self.on_po_entry_keypress)
        self.mode_clr_btn = builder.get_object("mode_clr_btn")
        self.mode_clr_btn.connect("color-set", self.on_clr_set)
        self.editing = False
        self.edit_cursor = 0

        self.offset = offset

        self.surface_hdr = None
        self.surface_body = None

        self.settings = settings
        self.font = "Monospace"
        self.fontsize = 24
        self.hexlen = 16
        self.scr_speed = 2
        self.cur_clr = (0.3, 1, 0.3, .3)
        self.cur_clr2 = (0.3, 0.3, 1, .3)
        self.line_interval = 1.4
        self.linegap = int(self.fontsize * self.line_interval)

        self.set_data(data_iter, lines, False)
        self.size_is_set = False
        self.comments = comments

        self.cur_pos = 0
        self.cur_len = 0
        self.cur_back = None
        self.highlights = [] # (clr, pos, len, src)
        self.sel_len_tooltip = False

        self.hdr.connect("draw", self.on_draw)
        self.hdr.connect('configure-event', self.on_configure)
        self.body.connect("draw", self.on_draw)
        self.body.connect('configure-event', self.on_configure)
        self.body_vadj.connect("value-changed", self.on_vadj_changed)
        self.apply_settings()
        self.show_all()

    def apply_settings(self, check=True):
        self.line_interval = self.settings["hexview/line_interval"]["value"] if "hexview/line_interval" in self.settings else 1.4
        self.max_shift = int(self.fontsize * 1.5)
        self.linegap = int(self.fontsize * self.line_interval)
        self.set_hexlen(self.settings["hexview/hexlen"]["value"] if "hexview/hexlen" in self.settings else 16)
        self.scr_speed = self.settings["hexview/scrolling_speed"]["value"] if "hexview/scrolling_speed" in self.settings else 2
        if "hexview/cur_clr" in self.settings:
            c = self.settings["hexview/cur_clr"]["value"]
            c.alpha = .3
        else:
            c = (0.3, 1, 0.3, .3)
        self.cur_clr = c
        if "hexview/cur_clr2" in self.settings:
            c = self.settings["hexview/cur_clr2"]["value"]
            c.alpha = .3
        else:
            c = (0.3, 0.3, 1, .3)
        self.cur_clr2 = c
        self.set_font_size(self.settings["hexview/fontsize"]["value"] if "hexview/fontsize" in self.settings else 24)

    def init_lines(self):
        # set initial lines
        if len(self.data) > 15:
            for i in range(len(self.data) // 16):
                self.lines.append({"off": i * 16})
            if len(self.data) % 16 > 0:
                self.lines.append({"off": i * 16 + 16})
        else:
            self.lines.append({"off": 0})
        self.lines.append({"off": len(self.data)})

    def set_data(self, data_iter, lines=None, redraw=True):
        if type(data_iter) == bytes:
            self.data = data_iter
            self.data_iter = None
        else:
            self.data = self.page.model.get_value(data_iter, 3)
            self.data_iter = data_iter
        if not lines:
            self.lines = []
            self.init_lines()
        else:
            self.lines = lines  # list of dicts {"off": -- offset for the line, "mode": (clr, w) -- stroke above the line}
        self.line_offsets = [x["off"] for x in self.lines] # cache of offsets
        self.cur_pos = 0
        self.cur_len = 0
        self.body_vadj.set_value(0)
        self.body_vadj.set_upper((len(self.lines) + 1.5) * self.linegap)
        if redraw:
            self.redraw()
            bw = self.body.get_window()
            if bw:
                bw.invalidate_rect(None, True)

    def on_scroll(self, w, e):
        if e.direction == Gdk.ScrollDirection.UP:
            self.body_vadj.set_value(max(0, self.body_vadj.get_value() - self.linegap * self.scr_speed))
        elif e.direction == Gdk.ScrollDirection.DOWN:
            self.body_vadj.set_value(min(self.body_vadj.get_upper(), self.body_vadj.get_value() + self.linegap  * self.scr_speed))
        return True

    def scroll_to(self, off, redraw=False, force=True):
        '''
        force=False: do not scroll if off is already on the screen
        '''
        if off < 0:
            y = 0
        else:
            fl = self.find_line(off)
            if fl is None:
                fl = len(self.lines) - 1
            y = fl * self.linegap
        av = self.body_vadj.get_value()
        ap = self.body_vadj.get_page_size()
        if y <= av:
            # moving up
            nv = y
        elif y > av and (force or y >= av + ap):
            # moving down
            nv = min(y, self.body_vadj.get_upper() - ap)
        else:
            nv = av
        if nv != av:
            self.body_vadj.set_value(nv)
        if redraw:
            # need to refresh
            self.redraw(self.body)
            self.body.get_window().invalidate_rect(None, True)

    def on_vadj_changed(self, w):
        self.redraw(self.body)
        self.body.get_window().invalidate_rect(None, True)

    def on_clr_set(self, w):
        # to get back to text after color selection
        if w == self.comment_clr_btn:
            self.comment_entry.grab_focus()
        elif w == self.mode_clr_btn:
            self.mode_spn_btn.grab_focus()

    def on_po_entry_keypress(self, w, e):
        w.grab_remove()
        kn = Gdk.keyval_name(e.keyval)
        if kn == "Return":
            row = self.find_line(self.cur_pos)
            comment = w.get_text()
            if w == self.comment_entry:
                clr = self.comment_clr_btn.get_color().to_floats()
                self.comment_po.popdown()
                if comment:
                    self.lines[row]["nb"] = clr, comment
                else:
                    del self.lines[row]["nb"]
            elif w == self.mode_spn_btn:
                clr = self.mode_clr_btn.get_color().to_floats()
                self.mode_po.popdown()
                if comment and int(comment) > 0:
                    self.lines[row]["mode"] = clr, int(comment)
                else:
                    del self.lines[row]["mode"]
            self.redraw(self.body)
            if self.data_iter:
                self.update_mod4()

    def on_btnmove(self, w, e):
        if not e.state & Gdk.ModifierType.BUTTON1_MASK:
            return
        avl = int(self.body_vadj.get_value() / self.linegap)
        apl = int(self.body_vadj.get_page_size() / self.linegap)
        dirty, row, col, cur_len = self.conv_coords(avl, e.x, e.y)
        # scrolling with selection
        if dirty:
            if row > avl + apl or row < avl:
                self.body_vadj.set_value(row * self.linegap)
            if self.cur_len != 0:
                self.sel_len_tooltip = True
            nc = self.lines[row]["off"] + col
            self.cur_len = nc - self.cur_pos
            self.redraw()
            # TODO: invalidate only impacted part?
            self.body.get_window().invalidate_rect(None, True)
            self.hdr.get_window().invalidate_rect(None, True)

    def on_btnrelease(self, w, e):
        self.sel_len_tooltip = False

    def conv_coords(self, avl, x, y):
        # convert screen coords to hexview char coords
        yposi, yposf = divmod(y, self.linegap)
        dirty = False
        row, col = None, None
        cur_len = None
        row = int(yposi) + avl
        if yposf > .12 * self.linegap and yposf < .98 * self.linegap and row < len(self.lines):
            linelen = self.get_line_len(row)
            shift = 0
            if row > 0:
                shift = self.lines[row].get("shift") or 0
            if shift:
                shift *= self.max_shift // 4 / self.x_adv
            xpos = x / self.x_adv
            if xpos < 9:
                # click on address: select whole row
                cur_len = linelen - 1
                col = 0
                dirty = True
            elif xpos > 9.5 + shift and xpos < 8.5 + linelen * 3 + shift:
                xc = divmod(int(xpos - 9.5 - shift), 3)
                if xc[1] < 2:
                    col = int(xc[0])
                    cur_len = 0
                    dirty = True
            elif int(xpos - self.hexlen * 3 - 10.5 - shift - self.max_shift/self.x_adv) in range(0, linelen + 1):
                # select in utf part
                col = int(xpos - self.hexlen * 3 - 10.5 - shift - self.max_shift/self.x_adv)
                cur_len = 0
                dirty = True
        return dirty, row, col, cur_len

    def on_btnpress(self, w, e):
        avl = int(self.body_vadj.get_value()/self.linegap)
        self.body.grab_focus()
        dirty, row, col, cur_len = self.conv_coords(avl, e.x, e.y)
        if dirty:
            self.cur_len = cur_len
            self.cur_pos = self.lines[row]["off"] + col
            self.redraw(self.body)
            self.body.get_window().invalidate_rect(None, True)
            self.redraw(self.hdr)
            self.hdr.get_window().invalidate_rect(None, True)

    def on_keyrelease(self, action, param):
        self.sel_len_tooltip = False

    def blink_lock(self, *args):
        if self.editing:
            self.page.app.windows["main"].stbar_lock.get_child().props.icon_name = 'changes-allow-symbolic'
            return
        if self.lock_blink_counter % 2:
            self.page.app.windows["main"].stbar_lock.get_child().props.icon_name = 'changes-prevent-symbolic'
        else:
            self.page.app.windows["main"].stbar_lock.get_child().props.icon_name = 'emblem-important'
        self.lock_blink_counter -= 1
        return self.lock_blink_counter

    def on_keypress(self, action, param):
        kn = Gdk.keyval_name(param.keyval)
        # print("OKP", kn, param.state)
        if self.settings["hexview/show_keypress"]["value"] and len(kn) == 1:
            txt = kn
            if param.state & Gdk.ModifierType.CONTROL_MASK:
                txt = "^" + kn
            self.page.app.update_msg(txt)
        dirty = False
        scroll = False
        datalen = len(self.data)
        avl = int(self.body_vadj.get_value()/self.linegap)
        apl = int(self.body_vadj.get_page_size()/self.linegap)
        vpos = self.find_line(self.cur_pos)
        if kn.lower() in "0123456789abcdef" and not param.state & Gdk.ModifierType.CONTROL_MASK:
            if self.editing:
                kval = int(kn, 16)
                value = self.data[self.cur_pos]
                cur_pos = self.cur_pos
                if self.edit_cursor:
                    new_value = (value & 0xf0) + kval
                    if self.cur_pos - 1 < len(self.data):
                        self.cur_pos += 1
                    self.edit_cursor = 0
                else:
                    new_value = (value & 0xf) + (kval << 4)
                    self.edit_cursor = 1
                if ((1, 0, 0, .4), cur_pos, 0, "editing") not in self.highlights:
                    self.highlights.append(((1, 0, 0, .4), cur_pos, 0, "editing"))
                self.data = self.data[:cur_pos] + struct.pack("<B", new_value) + self.data[cur_pos + 1:]
                dirty = True
            elif self.page.app.windows["main"].stbar_lock.get_visible():
                # blinking lock to warn that editing is not enabled
                self.page.app.windows["main"].stbar_lock.get_child().props.icon_name = 'emblem-important'
                self.lock_blink_counter = 6
                GLib.timeout_add(200, self.blink_lock)
                return
        elif kn in "qQ" and not self.editing:
            self.cur_len = 3
            if param.state & Gdk.ModifierType.SHIFT_MASK:
                self.cur_len = -3
            dirty = True

        elif kn in "oO" and not self.editing:
            self.cur_len = 7
            if param.state & Gdk.ModifierType.SHIFT_MASK:
                self.cur_len = -7
            dirty = True

        elif kn in "iI" and not self.editing:
            po = self.comment_po
            clr, comment = self.lines[vpos]["nb"] if "nb" in self.lines[vpos] else ((0, 0, .8), "")
            self.comment_clr_btn.set_rgba(Gdk.RGBA(*clr, 1.0))
            self.comment_entry.set_text(comment)
            r = Gdk.Rectangle()
            r.y = self.linegap * vpos
            r.x = 10.5 * self.x_adv
            po.set_relative_to(self.body)
            po.set_pointing_to(r)
            po.show_all()
            po.popup()

        elif self.data_iter and kn in "sS" and not self.editing:
            # self.data_iter --> limerest
            if self.cur_len: # we probably don't want to search for a single byte
                if self.cur_len >= 0:
                    s, e = self.cur_pos, self.cur_pos + self.cur_len + 1
                else:
                    s, e = self.cur_pos + self.cur_len, self.cur_pos + 1
                cmd.execute("?x%s" % utils.d2hex(self.data[s:e]), self.page.app.windows["main"])

        elif self.data_iter and (kn in ("less", "greater", "comma", "period") or (param.state & Gdk.ModifierType.CONTROL_MASK and ((kn in "vV" and self.page.app.hvclp) or kn in "xX"))):
            # self.data_iter --> limerest
            if kn in ("greater", "period", "v", "V"):
                self.cur_len = 0
                ext = 1
                if kn == "greater":
                    self.data += b"\x00"
                    self.cur_pos = max(0, len(self.data) - 1)
                elif kn in "vV":
                    shift = 0 if kn == "v" else 1
                    self.data = self.data[:self.cur_pos + shift] + self.page.app.hvclp + self.data[self.cur_pos + shift:]
                    ext = len(self.page.app.hvclp)
                else:
                    self.data = self.data[:self.cur_pos] + b'\x00' + self.data[self.cur_pos:]
                    self.cur_pos += 1
                self.page.model.set_value(self.data_iter, 3, self.data)
                self.lines[-1]["off"] += ext
                self.line_offsets[-1] += ext
                if self.get_line_len(len(self.lines) - 2) > max(16, self.hexlen):
                    off = self.lines[-2]["off"] + max(16, self.hexlen)
                    self.lines.insert(-1, {"off": off})
                    self.line_offsets.insert(-1, off)
            elif kn in ("less", "comma", "x", "X") and len(self.data) > 0:
                ext = 1
                if kn == "less" or (kn == "comma" and self.cur_pos > 0):
                    if kn == "less":
                        self.data = self.data[:-1]
                        self.cur_pos = max(0, len(self.data) - 1)
                    elif kn == "comma" and self.cur_pos > 0:
                        self.data = self.data[:self.cur_pos - 1] + self.data[self.cur_pos:]
                        self.cur_pos -= 1
                    self.cur_len = 0
                    self.page.model.set_value(self.data_iter, 3, self.data)
                    self.lines[-1]["off"] -= ext
                    self.line_offsets[-1] -= ext
                    if self.lines[-1]["off"] == self.lines[-2]["off"]:
                        self.lines.pop()
                        self.line_offsets.pop()
                elif kn in "xX":
                    ext = abs(self.cur_len) + 1
                    if self.cur_len >= 0:
                        self.page.app.hvclp = self.data[self.cur_pos:self.cur_pos + ext]
                        self.data = self.data[:self.cur_pos] + self.data[self.cur_pos + ext:]
                    else:
                        self.page.app.hvclp = self.data[self.cur_pos - ext + 1:self.cur_pos + 1]
                        self.data = self.data[:self.cur_pos - ext + 1] + self.data[self.cur_pos + 1:]
                        self.cur_pos -= ext - 1
                    self.cur_len = 0
                    self.page.model.set_value(self.data_iter, 3, self.data)
                    # FIXME: optimization is possible
                    # current side effect: reset line lengths
                    self.lines = []
                    self.init_lines()
                else:
                    return
            else:
                return
            if self.data_iter:
                self.update_mod4()
            vpos = self.find_line(self.cur_pos) or 0
            dirty = True
            if vpos < avl:
                self.body_vadj.set_value(int((vpos - 1) * self.linegap))
            elif vpos > avl + apl:
                self.body_vadj.set_value(int(vpos * self.linegap))

        elif kn == "x":
            # 'x' to swith back/forth between selection ends
            pass

        elif kn == "X" and not self.editing:
            # 'X' to invert selection
            self.cur_len *= -1
            self.cur_pos -= self.cur_len
            dirty = True

        elif kn == "Tab":
            shift = self.lines[vpos].get("shift") or 0
            if shift < 4:
                self.lines[vpos]["shift"] = shift + 1
                dirty = True
                if self.data_iter:
                    self.update_mod4()

        elif kn == "ISO_Left_Tab":
            shift = self.lines[vpos].get("shift") or 0
            if shift > 0:
                self.lines[vpos]["shift"] = shift - 1
                dirty = True
                if self.data_iter:
                    self.update_mod4()

        elif kn == "BackSpace":
            if vpos == 0:
                if self.cur_pos == 0:
                    # nothing to split
                    return
                # the only reasonable thing is to work same as 'Return' here
                self.lines.insert(1, {"off": self.cur_pos})
                self.line_offsets.insert(1, self.cur_pos)
            elif self.cur_pos == self.lines[vpos]["off"]:  # and self.cur_len == 0 ... think about it...
                # need to append whole row to the previous one
                self.lines.pop(vpos)
                self.line_offsets.pop(vpos)
            else:
                # need to append left part to the previous row (probably update self.hexlen)
                self.lines[vpos]["off"] = self.cur_pos
                self.line_offsets[vpos] = self.cur_pos
            dirty = True
            if self.data_iter:
                self.update_mod4()

        elif kn == "Return":
            if self.cur_pos == self.lines[vpos]["off"] and self.cur_len == 0:
                # adds 'mode' (separator line above)
                clr, w = self.lines[vpos].get("mode") if "mode" in self.lines[vpos] else ((1,0,0), None)
                if param.state & Gdk.ModifierType.SHIFT_MASK:
                    # bring 'mode' popover
                    po = self.mode_po
                    self.mode_clr_btn.set_rgba(Gdk.RGBA(*clr, 1.0))
                    self.mode_spn_btn.set_value(w or 1)
                    r = Gdk.Rectangle()
                    r.y = self.linegap * vpos
                    r.x = 10.5 * self.x_adv
                    po.set_relative_to(self.body)
                    po.set_pointing_to(r)
                    po.show_all()
                    po.popup()
                else:
                    if w:
                        w += 1
                        if w < 6:
                            self.lines[vpos]["mode"] = clr, w
                        else:
                            del self.lines[vpos]["mode"]
                    else:
                        self.lines[vpos]["mode"] = (1,0,0), 1
                    dirty = True
            else:
                # split the line
                if self.cur_len >= 0:
                    row = vpos
                    if self.cur_pos != self.lines[row]["off"]:
                        row += 1
                        self.lines.insert(row, {"off": self.cur_pos})
                        self.line_offsets.insert(row, self.cur_pos)
                    if self.cur_len > 0:
                        # double split if there is a selection
                        if row + 2 < len(self.lines):
                            req_off = min(self.cur_pos + self.cur_len + 1, datalen)
                            nextoff = self.lines[row + 1]["off"]
                            while nextoff <= req_off:
                                self.lines.pop(row + 1)
                                self.line_offsets.pop(row + 1)
                                if row + 2 > len(self.lines):
                                    break
                                nextoff = self.lines[row + 1]["off"]
                            if req_off <= datalen:
                                self.lines.insert(row + 1, {"off": req_off})
                                self.line_offsets.insert(row + 1, req_off)
                                self.cur_pos = req_off
                        if row > avl + apl:
                            self.body_vadj.set_value(int(row * self.linegap))
                else:
                    # need to swap ends
                    curpos = self.cur_pos + self.cur_len
                    curlen = -self.cur_len
                    row = self.find_line(curpos)
                    if curpos != self.lines[row]["off"]:
                        row += 1
                        self.lines.insert(row, {"off": curpos})
                        self.line_offsets.insert(row, curpos)

                    nextoff = self.lines[row + 1]["off"]
                    while nextoff <= self.cur_pos + 1:
                        self.lines.pop(row + 1)
                        self.line_offsets.pop(row + 1)
                        if row + 2 > len(self.lines):
                            break
                        nextoff = self.lines[row + 1]["off"]
                    if self.cur_pos + 1 <= datalen:
                        self.lines.insert(row + 1, {"off": self.cur_pos + 1})
                        self.line_offsets.insert(row + 1, self.cur_pos + 1)
                    self.cur_pos = max(0, curpos - 1)
                dirty = True
                if self.data_iter:
                    self.update_mod4()

        elif kn == "Escape" and self.cur_back:
            self.body_vadj.set_value(self.cur_back)
            return True

        elif kn == "Home":
            if self.cur_len >= 0:
                self.cur_pos = self.lines[vpos]["off"]
            else:
                row = self.find_line(self.cur_pos + self.cur_len)
                self.cur_pos = self.lines[row]["off"] - self.cur_len
            if self.editing:
                self.edit_cursor = 0
            dirty = True

        elif kn == "End":
            if self.cur_len >= 0:
                row = self.find_line(self.cur_pos + self.cur_len)
                linelen = self.get_line_len(row)
                self.cur_pos = self.lines[row]["off"] + linelen - self.cur_len - 1
            else:
                linelen = self.get_line_len(vpos)
                self.cur_pos = self.lines[vpos]["off"] + linelen - 1
            if self.editing:
                self.edit_cursor = 1
            dirty = True

        elif kn in "jJ" and not self.editing:
            # jump selection to the next/prev slot
            if kn == "j" and self.cur_pos + 2 * self.cur_len < datalen - 1:
                self.cur_pos += self.cur_len + 1
                dirty = True
            elif kn == "J" and self.cur_pos > self.cur_len:
                self.cur_pos -= self.cur_len + 1
                dirty = True

        elif kn == "Right":
            dirty = True
            selpos = -1
            if param.state & Gdk.ModifierType.SHIFT_MASK and not self.editing:
                if self.cur_len >= 0 and self.cur_pos + self.cur_len == datalen - 1:
                    return True
                if self.cur_pos + self.cur_len < datalen - 1:
                    self.cur_len += 1
                selpos = self.find_line(self.cur_pos + self.cur_len)
            else:
                if (self.cur_pos + self.cur_len == datalen - 1) and (not self.editing or self.edit_cursor):
                    return True
                diff = 1
                if param.state & Gdk.ModifierType.MOD1_MASK:
                    diff = abs(self.cur_len) + 1
                elif self.editing:
                    if self.edit_cursor:
                        self.edit_cursor = 0
                    else:
                        self.edit_cursor = 1
                        diff = 0
                self.cur_pos += diff
                if self.cur_pos > datalen - 1:
                    self.cur_pos = datalen - 1
                elif self.cur_len > 0 and self.cur_pos + self.cur_len > datalen - 1:
                    self.cur_pos = datalen - 1 - self.cur_len
                selpos = self.find_line(self.cur_pos)
            if selpos > avl + apl - 1:
                scroll = True
                scrollpos = max(self.body_vadj.get_value() + self.linegap, self.body_vadj.get_upper() - self.body_vadj.get_page_size())

        elif kn == "Left":
            dirty = True
            if param.state & Gdk.ModifierType.SHIFT_MASK and not self.editing:
                if self.cur_len < 0 and self.cur_pos + self.cur_len <= 0:
                    return
                self.cur_len -= 1
                if self.cur_pos + self.cur_len < 0:
                    self.cur_len = -self.cur_pos
                selpos = self.find_line(self.cur_pos + self.cur_len)
            else:
                if (self.cur_pos + self.cur_len == 0) and (not self.editing or not self.edit_cursor):
                    return
                diff = 1
                if param.state & Gdk.ModifierType.MOD1_MASK:
                    diff = abs(self.cur_len) + 1
                elif self.editing:
                    if self.edit_cursor:
                        self.edit_cursor = 0
                        diff = 0
                    else:
                        self.edit_cursor = 1
                self.cur_pos -= diff
                if self.cur_pos < 0:
                    self.cur_pos = 0
                elif self.cur_len < 0 and self.cur_pos + self.cur_len < 0:
                    self.cur_pos = -self.cur_len
                selpos = self.find_line(self.cur_pos)
            if selpos < avl:
                scroll = True
                scrollpos = min(self.body_vadj.get_value() - self.linegap, 0)

        elif kn == "Down":
            dirty = True
            row_s = vpos
            row_e = self.find_line(self.cur_pos + self.cur_len)
            col_s = self.cur_pos - self.lines[row_s]["off"]
            col_e = self.cur_pos + self.cur_len - self.lines[row_e]["off"]
            if self.cur_len < 0:
                row_s, row_e = row_e, row_s
                col_s = self.cur_pos + self.cur_len - self.lines[row_s]["off"]
                col_e =  self.cur_pos - self.lines[row_e]["off"]
            # row/col -- position to move from
            if (self.cur_len >= 0 and self.cur_pos + self.cur_len >= datalen - 1) or (self.cur_len < 0 and self.cur_pos >= datalen - 1):
                # already at the end of data
                return True
            if param.state & Gdk.ModifierType.SHIFT_MASK and not self.editing:
                # with shift we increase/decrease cur_len
                if self.cur_len >= 0:
                    if row_e + 2 >= len(self.lines):
                        self.cur_len = datalen - self.cur_pos - 1
                        newrow = row_e
                    else:
                        newrow = row_e + 1 if row_e + 1 < len(self.lines) else row_e
                        self.cur_len += self.get_line_len(row_e) - col_e + min(col_e, self.get_line_len(newrow) - 1)
                else:
                    if row_s + 2 >= len(self.lines):
                        self.cur_len = datalen - self.cur_pos - 1
                        newrow = row_e
                    else:
                        newrow = row_s + 1 if row_s + 1 < len(self.lines) else row_s
                        self.cur_len += self.get_line_len(row_s) - col_s + min(col_s, self.get_line_len(newrow) - 1)
            else:
                # we move cursor
                if self.cur_len >= 0:
                    max_shift = datalen - self.cur_pos - self.cur_len - 1
                    newrow = row_s + 1 if row_s + 1 < len(self.lines) else row_s
                    shift = min(col_s, self.get_line_len(newrow) - 1) + self.get_line_len(row_s) - col_s
                else:
                    max_shift = datalen - self.cur_pos - 1
                    newrow = row_e + 1 if row_e + 1 < len(self.lines) else row_e
                    shift = min(col_e, self.get_line_len(newrow) - 1) + self.get_line_len(row_e) - col_e
                self.cur_pos += min(shift, max_shift)
            if newrow > avl + apl - 1:
                scroll = True
                scrollpos = min(self.body_vadj.get_value() + self.linegap, self.body_vadj.get_upper() - self.body_vadj.get_page_size())

        elif kn == "Up":
            dirty = True
            row_s = vpos
            row_e = self.find_line(self.cur_pos + self.cur_len)
            col_s = self.cur_pos - self.lines[row_s]["off"]
            col_e = self.cur_pos + self.cur_len - self.lines[row_e]["off"]
            if self.cur_len < 0:
                row_s, row_e = row_e, row_s
                col_s = self.cur_pos + self.cur_len - self.lines[row_s]["off"]
                col_e =  self.cur_pos - self.lines[row_e]["off"]
            if (self.cur_len >= 0 and self.cur_pos <= 0) or (self.cur_len < 0 and self.cur_pos + self.cur_len <= 0):
                # already at the start of data
                return True
            if param.state & Gdk.ModifierType.SHIFT_MASK and not self.editing:
                # with shift we increase/decrease cur_len
                if self.cur_len >= 0:
                    newrow = max(0, row_e - 1)
                    col = min(col_e, self.get_line_len(newrow) - 1) if row_s else 0
                else:
                    newrow = max(0, row_s - 1)
                    col = min(col_s, self.get_line_len(newrow) - 1) if row_s else 0
                self.cur_len = self.lines[newrow]["off"] + col - self.cur_pos
            else:
                # we move cursor
                if self.cur_len >= 0:
                    max_shift = self.cur_pos
                    newrow = max(0, row_s - 1)
                    col = min(col_s, self.get_line_len(newrow) - 1)
                else:
                    max_shift = self.cur_pos + self.cur_len
                    newrow = row_e - 1 if row_e > 0 else 0
                    col = min(col_e, self.get_line_len(newrow) - 1)
                shift = self.cur_pos - self.lines[newrow]["off"] - col
                if row_s == 0:
                    shift = max_shift
                self.cur_pos -= min(shift, max_shift)
                selpos = 0 if self.cur_len < 0 else self.find_line(self.cur_pos) or 0
            self.body_vadj.set_value(int(self.body_vadj.get_value() - self.linegap))
            dirty = True

        elif kn == "Page_Down":
            row_s = vpos
            row_e = self.find_line(self.cur_pos + self.cur_len)
            col_s = self.cur_pos - self.lines[row_s]["off"]
            col_e = self.cur_pos + self.cur_len - self.lines[row_e]["off"]
            if self.cur_len < 0:
                row_s, row_e = row_e, row_s
                col_s = self.cur_pos + self.cur_len - self.lines[row_s]["off"]
                col_e =  self.cur_pos - self.lines[row_e]["off"]
            if (self.cur_len >= 0 and self.cur_pos + self.cur_len >= datalen - 1) or (self.cur_len < 0 and self.cur_pos >= datalen - 1):
                # already at the end of data
                return True
            if self.cur_len >= 0:
                max_shift = datalen - self.cur_pos - self.cur_len - 1
                newrow = row_s + apl - 1 if row_s + apl < len(self.lines) else len(self.lines) - row_e + row_s - 1
                col = min(col_s, self.get_line_len(newrow) - 1)
            else:
                max_shift = datalen - self.cur_pos - 1
                newrow = row_e + apl if row_e + apl < len(self.lines) else len(self.lines) - 1
                col = min(col_e, self.get_line_len(newrow) - 1)
            shift = self.lines[newrow]["off"] + col - self.cur_pos
            self.cur_pos += min(shift, max_shift)
            scrshift = apl//2
            if row_e != row_s and row_e - row_s < apl:
                scrshift = row_e - row_s
            self.body_vadj.set_value(max(0, newrow - apl + 1 + scrshift) * self.linegap)
            dirty = True

        elif kn == "Page_Up":
            row_s = vpos
            row_e = self.find_line(self.cur_pos + self.cur_len)
            col_s = self.cur_pos - self.lines[row_s]["off"]
            col_e = self.cur_pos + self.cur_len - self.lines[row_e]["off"]
            if self.cur_len < 0:
                row_s, row_e = row_e, row_s
                col_s = self.cur_pos + self.cur_len - self.lines[row_s]["off"]
                col_e =  self.cur_pos - self.lines[row_e]["off"]
            if (self.cur_len >= 0 and self.cur_pos <= 0) or (self.cur_len < 0 and self.cur_pos + self.cur_len <= 0):
                # already at the start of data
                return True
            if self.cur_len >= 0:
                max_shift = self.cur_pos
                newrow = max(0, row_s - apl + 1)
                col = min(col_s, self.get_line_len(newrow) - 1)
            else:
                max_shift = self.cur_pos + self.cur_len
                newrow = max(0, row_e - apl + 1)
                col = min(col_e, self.get_line_len(newrow) - 1)
            shift = self.cur_pos - self.lines[newrow]["off"] - col
            if row_s == 0:
                shift = max_shift
            self.cur_pos -= min(shift, max_shift)
            self.body_vadj.set_value(int(self.body_vadj.get_value() - (apl - 1) * self.linegap))
            dirty = True

        elif kn in "wW":
            curln_len = self.get_line_len(vpos)
            curln_off = self.lines[vpos]["off"]
            if kn == "w" and vpos > 0:
                # make equal to previous and go to next line
                prevln_len = self.get_line_len(vpos - 1)
                if curln_len == prevln_len and self.cur_pos + prevln_len < datalen:
                    self.cur_pos += prevln_len
                elif curln_len > prevln_len:
                    # need to split current line at prevln_len
                    # and move cur_pos to the next line
                    self.cur_pos = curln_off + prevln_len
                    self.lines.insert(vpos + 1, {"off": self.cur_pos})
                    self.line_offsets.insert(vpos + 1, self.cur_pos)
                elif curln_len < prevln_len:
                    # need to borrow from the next line(s)
                    if vpos + 2 >= len(self.lines):
                        # need to borrow on the last line
                        return
                    req_off = min(curln_off + prevln_len, datalen)
                    nextoff = self.lines[vpos + 1]["off"]
                    while nextoff <= req_off:
                        self.lines.pop(vpos + 1)
                        self.line_offsets.pop(vpos + 1)
                        nextoff = self.lines[vpos + 1]["off"]
                    self.lines.insert(vpos + 1, {"off": req_off})
                    self.line_offsets.insert(vpos + 1, req_off)
                    self.cur_pos = curln_off + prevln_len
                else:
                    return
                cur_line = self.find_line(self.cur_pos)
                if cur_line >= avl + apl:
                    self.body_vadj.set_value(int(self.body_vadj.get_value() + self.linegap))

            elif kn == "W" and vpos + 2 < len(self.lines):
                # make equal to next and go to previous line
                nextln_len = self.get_line_len(vpos + 1)
                if curln_len == nextln_len:
                    if self.cur_pos == curln_off and vpos > 0:
                        # we are at the start of the right len line
                        # need to go to the start of the line above
                        self.cur_pos = self.lines[vpos - 1]["off"]
                elif curln_len > nextln_len:
                    # need to split at nextln_len from the end
                    req_off = curln_off + curln_len - nextln_len
                    self.lines.insert(vpos + 1, {"off": req_off})
                    self.line_offsets.insert(vpos + 1, req_off)
                    self.cur_pos = curln_off
                elif curln_len < nextln_len and curln_off > 0:
                    # need to borrow from the line before
                    req_off = max(0, curln_off + curln_len - nextln_len)
                    reqln = self.find_line(req_off)
                    reqln_off = self.lines[reqln]["off"]
                    nextoff = self.lines[reqln + 1]["off"]
                    while nextoff <= curln_off:
                        self.lines.pop(reqln + 1)
                        self.line_offsets.pop(reqln + 1)
                        nextoff = self.lines[reqln + 1]["off"]
                    if reqln_off < req_off:
                        self.lines.insert(reqln + 1, {"off": req_off})
                        self.line_offsets.insert(reqln + 1, req_off)
                    self.cur_pos = max(0, reqln_off)
                else:
                    return
                cur_line = self.find_line(self.cur_pos)
                if cur_line < avl and avl > 0:
                    self.body_vadj.set_value(int(cur_line * self.linegap))
            dirty = True
            if self.data_iter:
                self.update_mod4()

        elif kn == "space":
            self.cur_back = self.body_vadj.get_value()
            self.body_vadj.set_value(vpos * self.linegap)
            return True

        elif kn in "aA" and param.state & Gdk.ModifierType.CONTROL_MASK and not self.editing: # select all"
            self.cur_pos = 0
            self.cur_len = datalen - 1
            dirty = True

        elif kn in "cC" and param.state & Gdk.ModifierType.CONTROL_MASK:
            clp = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
            if self.cur_len >= 0:
                s, e = self.cur_pos, self.cur_pos + self.cur_len + 1
            else:
                s, e = self.cur_pos + self.cur_len, self.cur_pos + 1
            if kn == "C":
                # automagically copy all
                s, e = 0, datalen - 1
            text = utils.d2hex(self.data[s:e], space=" ")
            clp.set_text(text, len(text))
            clp.store()
            self.page.app.hvclp = self.data[s:e]

        if dirty:
            if self.cur_len != 0:
                self.sel_len_tooltip = True
            if scroll:
                self.body_vadj.set_value(scrollpos)
            elif vpos < avl:
                self.body_vadj.set_value((vpos - 1) * self.linegap)
            elif vpos + 1 > avl + apl:
                self.body_vadj.set_value((vpos - apl + 1) * self.linegap)
            self.redraw(self.body)
            self.body.get_window().invalidate_rect(None, True)
            self.redraw(self.hdr)
            self.hdr.get_window().invalidate_rect(None, True)
        return True

    def set_font_size(self, fontsize=24):
        if fontsize != self.fontsize:
            self.fontsize = fontsize
            self.size_is_set = False
            self.redraw()
            hw = self.hdr.get_window()
            if hw:
                hw.invalidate_rect(None, True)
            bw = self.body.get_window()
            if bw:
                bw.invalidate_rect(None, True)

    def set_hexlen(self, hexlen=16):
        if hexlen != self.hexlen:
            self.hexlen = hexlen
            self.numlines = math.ceil(len(self.data)/self.hexlen)
            self.size_is_set = False
            self.redraw()
            hw = self.hdr.get_window()
            if hw:
                hw.invalidate_rect(None, True)
            bw = self.body.get_window()
            if bw:
                bw.invalidate_rect(None, True)

    def init_surface(self, widget):
        w = self.get_allocated_width()
        h = self.get_allocated_height()
        hh = self.hdr.get_allocated_height()
        if (widget is None or widget == self.hdr):
            if self.surface_hdr is not None:
                self.surface_hdr.finish()
                self.surface_hdr = None
            self.surface_hdr = cairo.ImageSurface(cairo.FORMAT_ARGB32, w, hh)
        hb = h - hh
        self.body_vadj.set_page_size(hb)
        if (widget is None or widget == self.body):
            if self.surface_body is not None:
                self.surface_body.finish()
                self.surface_body = None
            self.surface_body = cairo.ImageSurface(cairo.FORMAT_ARGB32, w, hb)
            if hb >= self.body_vadj.get_upper():
                self.scrbar.hide()
            else:
                self.scrbar.show()

    def do2_drawing(self, widget):
        hexupdate = self.update_hexlen()
        if hexupdate:
            self.hdr.get_window().invalidate_rect(None, True)
        if widget is None or widget == self.hdr or hexupdate:
            self.draw_hdr()
        if widget is None or widget == self.body:
            self.draw_body()
        return hexupdate

    def redraw(self, widget=None):
        self.init_surface(widget)
        if not self.size_is_set:
            self.set_size()
            self.size_is_set = True
        hexupdate = self.do2_drawing(widget)
        if widget is None or widget == self.hdr or hexupdate:
            self.surface_hdr.flush()
        if widget is None or widget == self.body:
            self.surface_body.flush()

    def on_configure(self, widget, event, data=None):
        self.redraw(widget)
        return True

    def set_font(self, ctx):
        ctx.select_font_face("Monospace", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
        ctx.set_font_size(self.fontsize)

    def set_size(self):
        ctx = cairo.Context(self.surface_hdr)
        self.set_font(ctx)
        self.x_adv = ctx.text_extents("O")[4]
        self.y_adv = int(self.fontsize * 1.2)
        self.linegap = int(self.fontsize * 1.4)
        # width in chars:
        # .5 gap + 8 addr + 1 gap + 3*hexlen-1 + 1 gap + hexlen + .5 gap
        # 8+1+1+ 4*hexlen
        self.hdr.set_size_request(self.x_adv * (11 + self.hexlen * 4), self.linegap)
        self.body_vadj.set_upper(self.linegap * (len(self.lines) + 1.5))

    def draw_hdr(self):
        ctx = cairo.Context(self.surface_hdr)
        w, h  = self.surface_hdr.get_width(), self.surface_hdr.get_height()
        self.set_font(ctx)
        ctx.set_source_rgb(.9, .9, .9)
        ctx.set_line_width(1)
        ctx.rectangle(0, 0, w, h)
        ctx.fill()
        ctx.set_source_rgb(0, 0, 0)
        ctx.move_to(self.x_adv * 9.5, self.y_adv)
        hdr = " ".join(["%02x" % x for x in range(self.hexlen)])
        ctx.show_text(hdr)
        ctx.move_to(0, int(h) - .5)
        ctx.line_to(w, int(h) - .5)
        ctx.move_to(int(self.x_adv * 9) + .5, 0)
        ctx.line_to(int(self.x_adv * 9) + .5, h)
        ctx.move_to(int(self.x_adv * (9 + self.hexlen*3)) + .5, 0)
        ctx.line_to(int(self.x_adv * (9 + self.hexlen*3)) + .5, h)
        ctx.stroke()
        ctx.move_to(int(self.x_adv * (10 + self.hexlen*3)) + .5 + self.max_shift, self.y_adv)
        ctx.show_text("%#x" % self.cur_pos)
        if self.cur_len:
            ctx.show_text(":%#x" % (self.cur_pos + self.cur_len))

    def get_line_len(self, line_num):
        if line_num  >= len(self.lines) or line_num < 0:
            return 0
        end = self.lines[line_num + 1]["off"] - 1 if line_num + 1 < len(self.lines) else len(self.data)
        return end + 1 - self.lines[line_num]["off"]

    def find_line(self, offset):
        if offset < 0 or offset > len(self.data):
            return
        pos = bisect_left(self.line_offsets, offset)
        before = self.line_offsets[pos - 1]
        after = self.line_offsets[pos]
        if offset < after:
            pos -= 1
        return pos

    def draw_selection(self, ctx, color, sel_pos, sel_len, src=None, len_tooltip=False, editing=False):
        ctx.set_source_rgba(*color)
        if sel_len < 0:
            sel_len *= -1
            sel_pos -= sel_len
        if sel_pos < 0 or sel_pos + sel_len > len(self.data) - 1:
            return
        avl = int(self.body_vadj.get_value()/self.linegap)
        apl = math.ceil(self.body_vadj.get_page_size()/self.linegap)
        start_off_line = self.find_line(sel_pos)
        end_off_line = self.find_line(sel_pos + sel_len)

        if end_off_line < avl:
            # selection is above the current top line
            return
        end = avl + apl
        if start_off_line > end:
            # selection is below the current bottom line
            return
        row_s = start_off_line
        if start_off_line < avl:
            row_s = avl
            col_s = 0
        else:
            col_s = sel_pos - self.lines[start_off_line]["off"]

        row_e = end_off_line
        if end_off_line > end:
            row_e = avl + apl
            col_e = self.get_line_len(avl + apl)
        else:
            col_e = sel_pos + sel_len - self.lines[end_off_line]["off"]

        row_s -= avl
        row_e -= avl
        x0 = int(self.x_adv * 9.5)
        xs = x0 + int(self.x_adv * col_s * 3)
        ys = int(self.linegap * (row_s + .2))
        xe = x0 + int(self.x_adv * col_e * 3)
        ye = int(self.linegap * (row_e + .2))

        if row_e > row_s:
            shift = self.lines[row_s + avl].get("shift") or 0
            if shift:
                shift *= self.max_shift // 4
            xlen = self.get_line_len(row_s + avl)
            xf = int(self.x_adv  * (9.5 + xlen * 3 - 1))
            ctx.rectangle(xs + shift, ys, xf - xs + 1.5, self.y_adv)
            ctx.rectangle(self.x_adv * (self.hexlen * 3 + 10.5 + col_s) + shift + self.max_shift, ys, int((xlen - col_s) * self.x_adv) + 1.5, self.y_adv)
            for i in range(1, int(row_e - row_s)):
                shift = self.lines[row_s + avl + i].get("shift") or 0
                if shift:
                    shift *= self.max_shift // 4
                xlen = self.get_line_len(row_s + avl + i)
                xf = int(self.x_adv  * (9.5 + xlen * 3 - 1))
                ctx.rectangle(x0 + shift, ys + self.linegap * i, xf - x0 + 1.5, self.y_adv)
                ctx.rectangle(int(self.x_adv  * (self.hexlen * 3 + 10.5)) + shift + self.max_shift, ys + self.linegap * i, xlen * self.x_adv + 1.5, self.y_adv)
            shift = self.lines[row_e + avl].get("shift") or 0
            if shift:
                shift *= self.max_shift // 4
            xlen = self.get_line_len(row_e + avl)
            ctx.rectangle(x0 + shift, ye, xe - x0 + 2 * self.x_adv + 1.5, self.y_adv)
            ctx.rectangle(int(self.x_adv  * (self.hexlen * 3 + 10.5)) + shift + self.max_shift, ye, int(self.x_adv * (col_e + 1)) + 1.5, self.y_adv)
        else:
            shift = self.lines[row_s + avl].get("shift") or 0
            if shift:
                shift *= self.max_shift // 4
            xlen = self.get_line_len(row_s + avl)
            if editing:
                ctx.rectangle(xs + shift + (self.edit_cursor * self.x_adv), ys, self.x_adv + 1.5, self.y_adv)
            else:
                ctx.rectangle(xs + shift, ys, xe - xs + 2 * self.x_adv + 1.5, self.y_adv)
            ctx.rectangle(int(self.x_adv  * (self.hexlen * 3 + 10.5 + col_s)) + shift + self.max_shift, ys, int(self.x_adv * (col_e - col_s + 1)) + 1.5, self.y_adv)
        ctx.fill()
        if len_tooltip:
            tt = "%d" % (sel_len + 1)
            if sel_len > 8:
                tt += "/%x" % (sel_len + 1)
            if abs(row_s - row_e) > 0:
                tt += " (%d)" % (abs(row_s - row_e) + 1)
            ctx.set_source_rgba(1, 1, 1, .65)
            if ye < self.linegap:
                ye += self.linegap
            ctx.rectangle(xe, ye - self.linegap, (len(tt) + 1)* self.x_adv, self.y_adv)
            ctx.fill()
            ctx.save()
            ctx.set_source_rgba(.6, 0, 0, 1)
            ctx.select_font_face("Monospace", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
            ctx.move_to(xe + self.x_adv * .8, ye - self.y_adv * .8)
            ctx.show_text(tt)
            ctx.restore()

    def draw_body(self):
        ctx = cairo.Context(self.surface_body)
        w, h  = self.surface_body.get_width(), self.surface_body.get_height()
        self.set_font(ctx)
        # clear the area
        ctx.set_source_rgb(.9, .9, .9)
        ctx.rectangle(0, 0, self.x_adv * 9, h)
        ctx.fill()
        ctx.set_source_rgb(.99, .99, .99)
        ctx.rectangle(self.x_adv * 9.5, 0, self.x_adv * (self.hexlen + .5) * 3 + self.max_shift, h)
        ctx.fill()
        ctx.set_source_rgb(.99, .99, .95)
        ctx.rectangle(self.x_adv * (10.5 + self.hexlen * 3) + self.max_shift, 0, self.x_adv * self.hexlen, h)
        ctx.fill()

        # draw lines between addr/hex and hex/asc
        ctx.set_line_width(1)
        ctx.set_source_rgb(0, 0, 0)
        ctx.move_to(int(self.x_adv * 9) + .5, 0)
        ctx.line_to(int(self.x_adv * 9) + .5, h)
        ctx.move_to(int(self.x_adv * (9 + self.hexlen*3)) + .5, 0)
        ctx.line_to(int(self.x_adv * (9 + self.hexlen*3)) + .5, h)
        ctx.stroke()
        # thin vertical line to split columns of every 4 bytes
        for x in range(0, (self.hexlen - 1)//4):
            ctx.set_source_rgb(0.5, 0.5, 0.5)
            ctx.move_to(int(self.x_adv * (21 + 12 * x)) + .5, 0)
            ctx.line_to(int(self.x_adv * (21 + 12 * x)) + .5, h)
            ctx.stroke()
            ctx.set_source_rgba(.5, .5, .5, .5)
            ctx.move_to(int(self.x_adv * (14.5 + self.hexlen*3 + 4 * x)) + self.max_shift + .5, 0)
            ctx.line_to(int(self.x_adv * (14.5 + self.hexlen*3 + 4 * x)) + self.max_shift + .5, h)
            ctx.stroke()

        ctx.set_source_rgb(0, 0, 0)
        # draw data bytes
        self.draw_data(ctx)

        # draw highlights
        for i in self.highlights:
            self.draw_selection(ctx, *i)

        # draw current cursor selection
        self.draw_selection(ctx, self.cur_clr if self.cur_len >= 0 else self.cur_clr2, self.cur_pos, self.cur_len, None, self.sel_len_tooltip, self.editing)

        # get data to update statusbar
        if self.cur_len >= 0:
            data = self.data[self.cur_pos: self.cur_pos + self.cur_len + 1]
        else:
            data = self.data[self.cur_pos + self.cur_len: self.cur_pos + 1]
        self.page.app.windows["main"].statusbar_update(data)

    def on_draw(self, widget, ctx):
        if widget == self.hdr and self.surface_hdr is not None:
            ctx.set_source_surface(self.surface_hdr, 0.0, 0.0)
            ctx.paint()
        elif widget == self.body and self.surface_body is not None:
            ctx.set_source_surface(self.surface_body, 0.0, 0.0)
            ctx.paint()
        return True

    def update_mod4(self):
        '''
        Stores self.lines in the model to be able to retrieve them.
        '''
        mod4 = self.page.model.get_value(self.data_iter, 2) or {}
        if not "hv3" in mod4:
            mod4["hv3"] = {}
        mod4["hv3"]["lines"] = self.lines
        self.page.model.set_value(self.data_iter, 2, mod4)
        path = self.page.model.get_path(self.data_iter)
        if not self.page.project:
            self.page.project = {"file":self.page.path, "sha": None, "desc": None, "paths":{}}
        if str(path) not in self.page.project["paths"]:
            self.page.project["paths"][str(path)] = None
        self.page.update_stbar_proj()
        self.page.app.windows["main"].stbar_proj.show()


    def update_hexlen(self):
        if len(self.data) < 17:
            return
        avl = int(self.body_vadj.get_value()/self.linegap)
        apl = math.ceil(self.body_vadj.get_page_size()/self.linegap)
        row_s = max(0, avl - 5)
        row_e = min(avl + apl + 5, len(self.lines))
        hexlen = max([self.get_line_len(x) for x in range(row_s, row_e)])
        # update vertical size
        self.body_vadj.set_upper(self.linegap * (len(self.lines) + 1.5))
        if hexlen != self.hexlen:
            self.hexlen = hexlen
            return True

    def draw_data(self, ctx):
        # calculate which part to draw
        avl = int(self.body_vadj.get_value()/self.linegap)
        apl = math.ceil(self.body_vadj.get_page_size()/self.linegap)
        for i in range(apl):
            # draw address
            ctx.set_source_rgb(0, 0, 0)
            ctx.move_to(self.x_adv * .5, self.linegap * i + self.y_adv)
            ctx.show_text("%08x" % self.lines[avl + i]["off"])

            shift = self.lines[avl + i].get("shift") or 0
            if shift:
                shift *= self.max_shift // 4
            linelen = self.get_line_len(avl + i)
            if shift:
                # need to clear 4-column lines
                ctx.set_source_rgb(.99, .99, .99)
                ctx.rectangle(self.x_adv * 10.5, self.linegap * i, self.x_adv * linelen * 3, self.linegap * 2 - self.y_adv)
                ctx.fill()
                ctx.set_source_rgb(.99, .99, .95)
                ctx.rectangle(self.x_adv * (11.5 + self.hexlen * 3) + self.max_shift + shift, self.linegap * i, self.x_adv * (linelen - 1), self.linegap * 2 - self.y_adv)
                ctx.fill()

                if avl + i - 1 >= 0:
                    shift_before = self.lines[avl + i - 1].get("shift") or 0
                    if shift_before:
                        shift_before *= self.max_shift // 4
                    prev_len = self.get_line_len(avl + i - 1)
                else:
                    shift_before = 0
                    prev_len = self.hexlen
                if avl + i + 1 < len(self.lines):
                    shift_after = self.lines[avl + i + 1].get("shift") or 0
                    if shift_after:
                        shift_after *= self.max_shift // 4
                else:
                    shift_after = shift
                ctx.set_line_width(1)
                ctx.set_source_rgb(0.5, 0.5, 0.5)

                for col in range(4, linelen + 1, 4):
                    x1 = int(self.x_adv * (9 + col * 3)) + .5
                    if col <= prev_len:
                        x1 += shift_before
                    x2 = int(self.x_adv * (9 + col * 3)) + .5 + shift
                    x3 = int(self.x_adv * (9 + col*3)) + .5 + shift_after
                    y = int(self.linegap * (avl + i)) + .5
                    if col == self.hexlen:
                        ctx.stroke()
                        ctx.set_source_rgb(0, 0, 0)
                    ctx.move_to(x1, y)
                    ctx.line_to(x2, y + self.linegap - self.y_adv)
                    ctx.line_to(x2, y + self.linegap)
                    ctx.line_to(x3, y + self.linegap * 2 - self.y_adv)
                    if col == self.hexlen:
                        break
                    xtoff = int(self.x_adv * (1.5 + (self.hexlen - col) * 3 + col))
                    ctx.move_to(x1 + xtoff + self.max_shift, y)
                    ctx.line_to(x2 + xtoff + self.max_shift, y + self.linegap - self.y_adv)
                    ctx.line_to(x2 + xtoff + self.max_shift, y + self.linegap)
                    ctx.line_to(x3 + xtoff + self.max_shift, y + self.linegap * 2 - self.y_adv)
                ctx.stroke()

            # draw mode line if any
            if "mode" in self.lines[avl + i]:
                clr, w = self.lines[avl + i]["mode"]
                ctx.move_to(self.x_adv * 9.1, self.linegap * i)
                ctx.line_to(self.x_adv * (10.5 + self.hexlen * 4) + self.max_shift + shift, self.linegap * i)
                ctx.set_source_rgb(*clr)
                ctx.set_line_width(w)
                ctx.stroke()

            # draw comments
            if "nb" in self.lines[avl + i]:
                clr, txt = self.lines[avl + i]["nb"]
                if not clr: clr = (0, 0, .8)
                ctx.set_source_rgb(*clr)
                ctx.move_to(self.x_adv * (11.5 + self.hexlen * 4) + shift + self.max_shift, self.linegap * i + self.y_adv)
                ctx.show_text(txt)
            else:
                ctx.set_source_rgb(0, 0, 0)
            ctx.set_line_width(1)
            # draw hex
            ctx.move_to(self.x_adv * 9.5 + shift, self.linegap * i + self.y_adv)
            # FIXME! use get_line_len
            if avl + i + 1 < len(self.lines):
                end = self.lines[avl + i + 1]["off"]
            else:
                end = len(self.data)
            dbytes = self.data[self.lines[avl + i]["off"]: end]
            if not dbytes:
                break
            utf = b""
            for b in dbytes:
                ctx.show_text("%02x " % int(b))
                if b < 32 or b > 126:
                    utf += b"\xC2\xB7"
                else:
                    utf += struct.pack("B", b)
            # draw asc
            ctx.set_source_rgb(0, 0, 0)
            ctx.move_to(self.x_adv * (10.5 + self.hexlen * 3) + shift + self.max_shift, self.linegap * i + self.y_adv)
            ctx.show_text(utf.decode("utf-8"))

# END
