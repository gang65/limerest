# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gdk, Gtk, GObject

from utils import debug

@Gtk.Template(filename="ui/widgets.ui")
class TabLabel(Gtk.Box):
    __gtype_name__ = "TabLabel"

    label = Gtk.Template.Child()
    close_btn = Gtk.Template.Child()
    def __init__(self, label, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label.set_markup(label)


class CellRendererColorBox(Gtk.CellRenderer):
    '''
    CellRenderer that draws a ColorButton-like picture and activates ColorChooserDialog.
    Properties:
        value -- Gdk.RGBA color
        column in the model to write data;
        key for the dict in the model or None to use the column itself
    '''
    value = GObject.Property(type=Gdk.RGBA)
    column = GObject.Property(type=GObject.TYPE_INT)
    key = GObject.Property(type=GObject.TYPE_STRING)

    def __init__(self):
        super().__init__()
        self.set_property('mode', 1) # activatable

    def do_set_property(self, pspec, value):
        setattr(self, pspec.name, value)

    def do_get_property(self, pspec):
        return getattr(self, pspec.name)

    def do_get_size(self, widget, cell_area):
        return (0, 0, 16, 48)

    def do_render(self, cr, widget, background_area, cell_area, flags):
        size = 32
        cr.translate(cell_area.x + 12, cell_area.y + (cell_area.height - size) // 2)
        cr.rectangle(0, 0, size, size)
        cr.set_source_rgb(self.value.red, self.value.green, self.value.blue)
        cr.fill_preserve()
        cr.set_source_rgb(.9, .9, .9)
        cr.set_line_width(4)
        cr.stroke()

    def do_activate(self, event, w, path, background_area, cell_area, flags):
        model = w.get_model()
        selection = w.get_selection()
        _, itr = selection.get_selected()
        if not itr:
            return
        if str(path) != str(model[itr].path):
            return
        dlg = Gtk.ColorChooserDialog(title="Select color", parent=w.get_toplevel())
        dlg.set_rgba(self.value)
        res = dlg.run()
        if res and res == -5: # "Select/OK"
            rgba = dlg.get_rgba()
            if self.key is not None:
                model[path][self.column][self.key] = rgba
            else:
                model[path][self.column] = rgba
        dlg.destroy()
        return True

GObject.type_register(CellRendererColorBox)

