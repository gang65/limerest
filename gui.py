#!/usr/bin/env python3
#
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

'''
Classes and functions to reuse in GUI-related parts.
'''
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gio, Gtk, Gdk, GObject
import importlib
import os

import cmd
import utils
import widgets
from utils import debug

@Gtk.Template(filename="ui/window.ui")
class AppWindow(Gtk.ApplicationWindow):
    __gtype_name__ = "AppWindow"

    main_nb = Gtk.Template.Child()
    stbar1 = Gtk.Template.Child()
    stbar2 = Gtk.Template.Child()
    stbar3 = Gtk.Template.Child()
    stbar_menu = Gtk.Template.Child()
    cmd_entry = Gtk.Template.Child()
    stbpo_le_chkbox = Gtk.Template.Child()
    stbpo_be_chkbox = Gtk.Template.Child()
    stbpo_divonly_chkbox = Gtk.Template.Child()
    stbpo_div_entry = Gtk.Template.Child()
    stbpo_date_chkbox = Gtk.Template.Child()
    stbpo_date_entry = Gtk.Template.Child()
    stbpo_clr_chkbox = Gtk.Template.Child()
    stbpo_midi_chkbox = Gtk.Template.Child()
    stbpo_txt_chkbox = Gtk.Template.Child()
    stbpo_txt_entry = Gtk.Template.Child()
    stbpo_ipv4_chkbox = Gtk.Template.Child()
    stbar_proj = Gtk.Template.Child()
    proj_title = Gtk.Template.Child()
    proj_desc = Gtk.Template.Child()
    proj_tview = Gtk.Template.Child()
    cr_proj_comment = Gtk.Template.Child()
    stbar_lock = Gtk.Template.Child()

    def __init__(self, page, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.AppPage = page
        # This will be in the windows group and have the "win" prefix
        max_action = Gio.SimpleAction.new_stateful(
            "maximize", None, GLib.Variant.new_boolean(False)
        )
        self.app = kwargs['application']
        max_action.connect("change-state", self.on_maximize_toggle)
        self.add_action(max_action)

        self.cmd_history = []
        self.cmd_cur = -1
        # Keep it in sync with the actual state
        self.connect(
            "notify::is-maximized",
            lambda obj, pspec: max_action.set_state(
                GLib.Variant.new_boolean(obj.props.is_maximized)
            ),
        )
        self.search_tables = {}
        self.statusbar_data = None
        self.cmd_entry.connect("activate", cmd.on_cmd_entry_activate, self)
        self.cmd_entry.connect ("key-press-event", cmd.on_cmd_entry_keypressed, self)
        self.stbar_menu.connect("clicked", self.on_stbar_menu_clicked)
        self.stbar_lock.connect("clicked", self.on_stbar_lock_clicked)
        self.main_nb.connect("switch-page", self.on_page_switched)

        self.stbar_proj.get_popover().get_child().show_all()
        self.proj_tview.connect("row-activated", self.on_proj_row_activated)
        self.proj_tview.connect("key-press-event", self.on_proj_row_keypressed)
        self.cr_proj_comment.connect("edited", self.on_proj_comment_edited)
        self.apply_settings()

    def apply_settings(self):
        # Map app.settings to options
        option_map_chkbox = {
            "statusbar/show_le": self.stbpo_le_chkbox,
            "statusbar/show_be": self.stbpo_be_chkbox,
            "statusbar/show_midi": self.stbpo_midi_chkbox,
            "statusbar/show_color": self.stbpo_clr_chkbox,
            "statusbar/div_only": self.stbpo_divonly_chkbox,
            "statusbar/show_txt": self.stbpo_txt_chkbox,
            "statusbar/show_ipv4": self.stbpo_ipv4_chkbox,
        }
        for cbk, cbv in option_map_chkbox.items():
            cbv.set_active(bool(self.app.settings[cbk]["value"]))
            cbv.connect("toggled", self.on_stbpo_chkbox_toggled)

        option_map_entry = {
            "statusbar/divs": self.stbpo_div_entry,
            "statusbar/encoding": self.stbpo_txt_entry,
        }
        for cbk, cbv in option_map_entry.items():
            cbv.set_text(self.app.settings[cbk]["value"])
            cbv.connect("activate", self.on_stbpo_chkbox_toggled) # same outcome

    def on_page_switched(self, nb, page, pn):
        if self.app.appname != "limerest":
            return
        if page.project:
            page.update_stbar_proj()
            self.stbar_proj.show()
        else:
            self.stbar_proj.hide()
            self.app.saveproj_action.set_enabled(False)
        ftype = page.kwargs.get("type")
        if ftype in ["WMF", "EMF"]:
            self.app.add_rec_action.set_enabled(True)
            self.stbar_lock.set_visible(True)
        else:
            self.app.add_rec_action.set_enabled(False)
            self.stbar_lock.set_visible(False)

        fmt = ftype.lower()
        fmtpath = os.path.join(self.app.execpath, "formats", "%s.py" % fmt)
        if fmt and os.path.exists(fmtpath) and fmt not in self.app.formats:
            try:
                mod = importlib.import_module("formats.%s" % fmt)
                self.app.formats[fmt] = mod
            except Exception as e:
                print("Failed to import module", fmt, e)

        if fmt in self.app.formats and "file_save" in self.app.formats[fmt].__dict__:
            self.app.dump_action.set_enabled(True)
        else:
            self.app.dump_action.set_enabled(False)
        if page.path and os.path.exists(page.path):
            self.app.reload_action.set_enabled(True)
        else:
            self.app.reload_action.set_enabled(False)

    def on_proj_comment_edited(self, cr, prpath, new_text):
        prmodel = self.proj_tview.get_model()
        prmodel[prpath][2] = new_text
        pn = self.main_nb.get_current_page()
        if pn != -1:
            page = self.main_nb.get_nth_page(pn)
            page.project["paths"][prmodel[prpath][0]] = new_text

    def on_proj_row_keypressed(self, w, param):
        kn = Gdk.keyval_name(param.keyval)
        selection = w.get_selection()
        model, paths = selection.get_selected_rows()
        if not paths:
            return
        pn = self.main_nb.get_current_page()
        if pn < 0:
            return
        page = self.main_nb.get_nth_page(pn)
        if kn in ["BackSpace", "Delete"]:
            for path in paths:
                itr = model.get_iter(path)
                ppath = model.get_value(itr, 0)
                model.remove(itr)
                del page.project["paths"][ppath]
            return True

    def on_proj_row_activated(self, tv, path, tvc):
        prmodel = tv.get_model()
        v = prmodel[path][0]
        pn = self.main_nb.get_current_page()
        if pn != -1:
            page = self.main_nb.get_nth_page(pn)
            gtp = Gtk.TreePath.new_from_string(v)
            page.main_tview.expand_to_path(gtp)
            page.main_tview.set_cursor(gtp, None, False)
            page.main_tview.row_activated(gtp, page.main_tview.get_column(0))

    def on_stbpo_chkbox_toggled(self, w, *args, **kwargs):
        self.statusbar_update(self.statusbar_data)

    def on_stbar_menu_clicked(self, a):
        pass

    def on_stbar_lock_clicked(self, a):
        pnum = self.main_nb.get_current_page()
        if pnum < 0:
            return
        page = self.main_nb.get_nth_page(pnum)
        if page.hexview.editing:
            page.hexview.editing = False
            self.stbar_lock.get_child().props.icon_name = 'changes-prevent-symbolic'
            page.hexview.body.grab_focus()
            # need to store the changes
            if page.hexview.data_iter:
                page.model[page.hexview.data_iter][3] = page.hexview.data
            for hl in list(page.hexview.highlights):
                if len(hl) > 3 and hl[3] == "editing":
                    page.hexview.highlights.remove(hl)
        else:
            page.hexview.editing = True
            page.hexview.cur_len = 0
            self.stbar_lock.get_child().props.icon_name = 'changes-allow-symbolic'
            page.hexview.body.grab_focus()
        page.hexview.redraw()
        page.hexview.body.get_window().invalidate_rect(None, True)

    def statusbar_update(self, data, **kwargs):
        self.statusbar_data = data
        pnum = self.main_nb.get_current_page()
        ps_tables = []
        if pnum != -1:
            page = self.main_nb.get_nth_page(pnum)
            ps_tables = page.search_tables

        options =  {"le": self.stbpo_le_chkbox.get_active(),
                    "be": self.stbpo_be_chkbox.get_active(),
                    "clr": self.stbpo_clr_chkbox.get_active(),
                    "ipv4": self.stbpo_ipv4_chkbox.get_active(),
                    "midi": self.stbpo_midi_chkbox.get_active(),
                    "txt": self.stbpo_txt_chkbox.get_active(),
                    "txt_enc": self.stbpo_txt_entry.get_text().strip(),
                    "date": self.stbpo_date_chkbox.get_active(),
                    "date_delta": self.stbpo_date_entry.get_text().strip() or 11644473600, # Indd delta FIXME! UI?
                    "div_only": self.stbpo_divonly_chkbox.get_active(),
                    "divs": self.stbpo_div_entry.get_text(),
                    "ds_tables": self.search_tables,
                    "ps_tables": ps_tables}
        markup, txt = utils.data2status(data, options)
        if txt and len(txt) < 8:
            markup += " " + txt
        self.stbar2.set_markup(markup)

    def on_maximize_toggle(self, action, value):
        action.set_state(value)
        if value.get_boolean():
            self.maximize()
        else:
            self.unmaximize()

    def on_close_tab(self, tablabel, page):
        pn = self.main_nb.page_num(page)
        if pn > -1:
            self.main_nb.remove_page(pn)
        pn = self.main_nb.get_current_page()
        dirty = True
        if pn > -1 and self.main_nb.get_nth_page(pn).project:
            dirty = False
        if dirty:
            self.app.windows["main"].stbar_proj.hide()

    def change_page_name(self, page, name, color=None):
        if color:
            name = "<span foreground='%s'>" % color + name + "</span>"
        self.main_nb.get_tab_label(page).label.set_markup(name)
        self.main_nb.set_menu_label_text(page, name)

    def add_file(self, path, **kwargs):
        page = self.AppPage(path, self.app, **kwargs)
        name = os.path.split(path)[1]
        tablabel = widgets.TabLabel(label=name)
        tablabel.close_btn.connect("clicked", self.on_close_tab, page)
        pn = self.main_nb.append_page(page, tablabel)
        self.main_nb.set_menu_label_text(page, name)
        self.main_nb.set_tab_reorderable(page, True)
        if pn > -1:
            self.main_nb.set_current_page(pn)
        return page


def on_row_keypress(w, e, app=None):
    '''
    Better (IMHO) TreeView navigation with left/right/shift-down/shift-up.
    Also copies to clipboard iter path for selected iter (if any).
    '''
    kn = Gdk.keyval_name(e.keyval)
    if app:
        pn = app.windows["main"].main_nb.get_current_page()
        page = app.windows["main"].main_nb.get_nth_page(pn)
    selection = w.get_selection()
    model, itrs = selection.get_selected_rows()
    if not itrs:
        return
    itr_first = itrs[0]
    if e.state & Gdk.ModifierType.CONTROL_MASK and kn in "cCxX":
        clp = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        clp.set_text('"%s"' % itr_first, len(str(itr_first)) + 2)
        clp.store()
        if app:
            app.clp = []
            for i in itrs:
                app.clp.append(model[i][:])
            if kn in "xX" and page.kwargs["type"] in ["WMF", "EMF"]:
                for i in reversed(itrs):
                    del model[i]
    elif kn == "Delete" and page.kwargs["type"] in ["WMF", "EMF"]:
        for i in reversed(itrs):
            del model[i]
    elif e.state & Gdk.ModifierType.CONTROL_MASK and kn in "vV" and app and app.clp and page.kwargs["type"] in ["WMF", "EMF"]:
        if itr_first == model["0"].path:
            parent, itr = model[itr_first].iter, None
        else:
            parent, itr = None, model[itr_first].iter
        for i in app.clp:
            itr = model.insert_after(parent, itr, i)
    elif kn == "Right":
        if itr_first and not w.row_expanded(itr_first):
            w.expand_row(itr_first, False)
        return True
    elif kn == "Left":
        if itr_first:
            if w.row_expanded(itr_first):
                w.collapse_row(itr_first)
            else:
                parent = model[itr_first].parent
                if parent:
                    w.set_cursor(parent.path)
            return True
    elif kn == "Down" and e.state & Gdk.ModifierType.MOD1_MASK:
        itr2 = model.iter_next(model[itr_last].iter)
        if itr2:
            w.set_cursor(model[itr2].path)
            return True
    elif kn == "Up" and e.state & Gdk.ModifierType.MOD1_MASK:
        pos = itr_first[-1]
        if pos != 0:
            itr2_path = list(itr_first)[:-1]
            itr2_path.append(pos - 1)
            prev = model.get_iter(tuple(itr2_path))
            w.set_cursor(model[prev].path)
            return True

# END
